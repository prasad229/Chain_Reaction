#include<stdio.h>
#include<stdlib.h>
#include "protocols.h"
void comp(sq game[R][C]){
	printf("you must Quit and Restart the game before start playing with comp.\n");
	compstart(game);
	int i = 1, r, c, flag, men, cont = 0 ; 
	char ch;
	compt move;
	flag = 0;
		while(i){
		  	if(i % 2 == 1){
				if(existR(game)){
					flag = 0;
					ch = 'R';
					printf("Press -1 to go to the Menu:\n");
					printf(ANSI_COLOR_RED "color : %ced\n", ch);				
					printf(ANSI_COLOR_RED "row :  ");
					scanf("%d", &r);
					printf(ANSI_COLOR_RED "coloumn :" ANSI_COLOR_RESET "\n");
					scanf("%d", &c);
					flag = compput(game, ch, r, c);
					//printf("flag %d\n", flag);
					if(flag == 1)
						print(game, 'B');
					else 	print(game, 'R');
					i = i + (flag % 2);
				}
			else {
				printf(ANSI_COLOR_BLUE "%s (BLUE Team) Has Won!!! " ANSI_COLOR_RESET "\n", play2);
				menu(game, ' '); 
			     }
			}
		if(i % 2 == 0){
			if(existB(game)){
				flag = 0;
				ch = 'B';
				move = other4(game);
				if(move.r == 0){
					move = corn0(game);
					//printf("Used corn 0\n");
				}
				if(move.r == 0){
					move = other3(game);
					//printf("Used other 3\n");
				}
				if(move.r == 0){
					//move.r = move.c = 3;
					move = myrandom(game);
					//printf("Used random\n");
				}
				printf("used other 4\n"); 
				flag = compput(game, ch, move.r, move.c);
				//printf("flag %d\n", flag);
				if(flag == 1)
					print(game, 'R');
				else print(game, 'B');	
				i = i + (flag % 2);
			}
			else {
				printf(ANSI_COLOR_RED "%s (RED Team) Has Won!!!" ANSI_COLOR_RESET "\n", play1);
				menu(game, ' ');
			}
		}
	}

	
}
compt other3(sq game[R][C]){   
    //This functions checks if there any edge element is 2 and ready to split.
    compt tmp;
	tmp.r = tmp.c = 0;
	int dec = R - game[1][1].dec;
	int i, j;
	for(i = 2; i <= R - 1 - dec; i++){
		if( game[1][i].no == 2 && game[1][i].col == 'B'){
			tmp.r = 1;
			tmp.c = i;
			return tmp;
		}
		else if( game[R-dec][i].no == 2 && game[R-dec][i].col == 'B'){
			tmp.r = R - dec;
			tmp.c = i;
			return tmp;
		}
		else if( game[i][C - dec].no == 2 && game[i][C - dec].col == 'B'){
			tmp.r = i;
			tmp.c = C - dec;
			return tmp;
		}
		else if( game[i][1].no == 2 && game[i][1].col == 'B'){
			tmp.r = i;
			tmp.c = 1;
			return tmp;
		}
	}

	return tmp;
}
int total(sq game[R][C]){   //This gives the total no of orbs in game.
	int dec = R - game[1][1].dec;
	int sum = 0, i, j;
	for(i = 1; i <= R-dec; i++)
		for(j = 1; j <= C-dec; j++){
			sum += game[i][j].no;
		}
	printf("sum is %d\n",sum);
	return sum;	
}
compt myrandom(sq game[R][C]){
    //This function gives the random co-ordinates for comp to play...
	compt tmp;
	tmp.r = tmp.c = 0;
	int dec = R - game[1][1].dec;
	 int i, j ,flag = 1, rand, rand1, rand2;
	rand = total(game);
	rand1 = rand * 31 + 93;
	rand2 = rand * 7 + 61;
	printf("rand1 is %d\n", rand1);
	printf("rand2 is %d\n", rand2);
	while(flag){
		tmp.r = (rand1 % (R-dec)) + 1;
		tmp.c = (rand2 % (C-dec)) + 1;
		printf("tmp.r is %d\n", tmp.r);
		printf("tmp.c is %d\n", tmp.c);
	
		if(game[tmp.r][tmp.c].col != 'R')
			flag = 0;
	}
	return tmp;
}
compt corn0(sq game[R][C]){
    /*
    *This Condition is checked first if there is any vacant corner , comp player occupies that corner by adding
    *   its orb to that position.
    */
	compt tmp;
	int dec = R - game[1][1].dec;	
	tmp.r = tmp.c = 0;
	if(game[1][C-dec].no == 0){
		tmp.r = 1;
		tmp.c = C-dec;
	}
	else if(game[R-dec][C-dec].no == 0){
		tmp.r = R-dec;
		tmp.c = C-dec;
	}
	else if (game[R-dec][1].no == 0){
		tmp.r = R-dec;
		tmp.c = 1;
	}
	return tmp;
}
compt other4(sq game[R][C]){
    //Tor Comp Play If There Is Any 3 orb of comp color, it gives comp move such that 3 splits.
	compt tmp;
	tmp.r = tmp.c = 0;
	int dec = R - game[1][1].dec;
	int i, j;
	for(i = 1; i <= R-dec; i++ )
		for(j = 1; j <= C-dec; j++){
			if(game[i][j].no == 3 && game[i][j].col == 'B'){
				tmp.r = i;
				tmp.c = j;
				return tmp;
			}
		}
	return tmp;
}
int compput(sq game[R][C], char col, int r, int c){
    //This function checks whether comp has put in the right position and if yes then changes the array to new state...
	int dec = R - game[1][1].dec;
	if((col == 'R' || col == 'B') && (r <= R-dec && c <= C-dec && r > 0 && c > 0)){
		int m, n, chk = 0;
		m = r; n = c;
		if(col == 'B'){
			if ( !(isR(game, m, n))){
				(game[r][c]).col = col;
	    			(game[r][c]).no++;
				char chang = 'B';
				split4(game, chang);
				return 1;
			}
			else 
				return 0;
		}
		if(col == 'R'){
			if ( !(isB(game, m, n))){
				(game[r][c]).col = col;
	    			(game[r][c]).no++;
				char changR = 'R';
				split4(game, changR);
				return 1;
			}
			else {
				printf(ANSI_COLOR_RED "already other color exist plz choose different position" ANSI_COLOR_RESET "\n");
				return 0;
			}
		}
	}
	else if( r == -1 || c == -1){
		menu(game, col);
	}	
	else if(col == 'R'){
		printf("Out Of Range!!! Please Enter Position Between 1 to %d only!\n", R-dec);
		return 0;
	}
}
void compstart(sq game[R][C]){
      	int i, j, r, c, flag = 0, dimflag = 1;
	char ch1, ch2;
	printf(ANSI_COLOR_G "Enter the dimensions of the board:\n" ANSI_COLOR_RESET);
	while(dimflag){
		scanf("%d", &game[1][1].dec);
		dimflag = 0;
		if(game[1][1].dec < 2 || game[1][1].dec > 15){
			printf(ANSI_COLOR_Y "Dimensions should be between 2 to 15 for better gaming experience.\n Enter Again!\n" ANSI_COLOR_RESET);
			dimflag = 1;
		}
	}
	//scanf("%d", &game[1][1].dec);
	//scanf("%d", &game[1][1].dec);
	int dec = R - game[1][1].dec;
    	
    	for (i = 1; i <= R-dec; i++){
        	for(j = 1; j <= C-dec; j++){
        	    	(game[i][j].no) = 0;
			(game[i][j].col) = ' ';
       		 }
    	}
	printf(ANSI_COLOR_RED "Enter Player1's Name :-\n" ANSI_COLOR_RESET);
	scanf("%s", play1);
	printf(ANSI_COLOR_BLUE "Player2's Name is Computer .\n" ANSI_COLOR_RESET);	
	print(game, 'R');
	printf("%s is Red Team:\n", play1);
	//first chance to make player1 move(Red)...
		ch1 = 'R';
		printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
		printf(ANSI_COLOR_RED "row :  ");
		scanf("%d", &r);
		printf(ANSI_COLOR_RED "coloumn : " ANSI_COLOR_RESET "\n");
		scanf("%d", &c);
	flag = put(game, ch1, r, c);
	while(!flag){
		printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
		printf(ANSI_COLOR_RED "row :  ");
		scanf("%d", &r);
		printf(ANSI_COLOR_RED "coloumn :  " ANSI_COLOR_RESET"\n");
		scanf("%d", &c);
		flag = put(game, ch1, r, c);
	}
	print(game, 'B' );
	// Second chance to make player2 move(Blue)...
		printf(ANSI_COLOR_BLUE "Computer is Blue Team:\n");
		ch2 = 'B';
		if(game[1][1].no == 0){
			r = 1;
			c = 1;
		}
		else r = c = R-dec;
	flag  = put(game, ch2, r, c);
	print(game, 'R' );
}

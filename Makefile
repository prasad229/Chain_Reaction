project: main.o comp.o newchain.o newestsplit.o savegame.o
	cc main.o comp.o newchain.o newestsplit.o savegame.o -o project
main.o: main.c protocols.h newchain.c newestsplit.c comp.c
	cc -c main.c
newchain.o: newchain.c protocols.h newestsplit.c savegame.c
	cc -c newchain.c
newestsplit.o: protocols.h newchain.c newestsplit.c 
	cc -c newestsplit.c
savegame.o: protocols.h savegame.c newchain.c newestsplit.c
	cc -c savegame.c
comp.o: protocols.h newchain.c newestsplit.c comp.c
	cc -c comp.c

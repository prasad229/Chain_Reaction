#define R 15
#define C 15
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_G   "\x1b[32m"
#define ANSI_COLOR_Y   "\x1b[33m"
#define ANSI_COLOR_RESET     "\x1b[0m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
typedef struct sq{
    char col;
    int no;
    int dec;
}sq;
typedef struct compt{
	int r, c;
}compt;
char play1[32], play2[32];
//Function Prototypes..
int isR(sq game[R][C], int r, int c);
int isB(sq game[R][C], int r, int c);
int put(sq game[R][C], char col, int r, int c);
int existR(sq game[R][C]);
int existB(sq game[R][C]);
void print(sq game[R][C], char col);
void start(sq game[R][C]);
void split4(sq game[R][C], char col);
int menu(sq game[R][C], char col);
void Exit(sq game[R][C], char col);
void save(sq game[R][C], char col);
int load(sq game[R][C]);
int continu(sq game[R][C], char col);
void comp(sq game[R][C]);
void compstart(sq game[R][C]);
int compput(sq game[R][C], char col, int r, int c);
compt corn0(sq game[R][C]);
compt other4(sq game[R][C]);
compt other3(sq game[R][C]);
compt myrandom(sq game[R][C]);
int total(sq game[R][C]);
#include<stdio.h>
#include<stdlib.h>
#include "protocols.h"
void split4(sq game[R][C], char col){
	int flag = 0, i, j, k, l, extra, r, c;
	int dec = R - game[1][1].dec;
	long int safe = 1;
	while(!flag && safe < 100000){
		safe++;
              	flag = 1;
               	for(i = 1; i <= R-dec; i++){
                	for(j = 1; j <= C-dec; j++){
				if( (i == 1 && j == 1) || (i == R-dec && j == 1) || ( i == 1 && j == C-dec) || (i == R-dec && j == C-dec)){
					if(game[i][j].no > 1){
                        			flag = 0;
             				        extra = game[i][j].no / 2;
						r = i; c  = j;
		        	                /*game[i - 1][j].no += extra;
						game[i - 1][j].col  = col;
                			        game[i + 1][j].no += extra;
						game[i + 1][j].col = col;
                			        game[i][j - 1].no += extra;
						game[i][j - 1].col = col;
                			        game[i][j + 1].no += extra;
						game[i][j + 1].col = col;*/
						if(r == 1 && c == 1){						
							(game[r+1][c]).col = col;
							(game[r + 1][c]).no += extra;
							(game[r][c + 1]).col = col;
							(game[r][c + 1]).no += extra;						
						}
						if(r == R-dec && c == 1){						
							(game[r - 1][c]).col = col;
							(game[r - 1][c]).no += extra;
							(game[r][c + 1]).col = col;
							(game[r][c + 1]).no += extra;						
						}
						if(r == 1 && c == C-dec){						
							(game[r][c - 1]).col = col;
							(game[r][c - 1]).no += extra;
							(game[r + 1][c]).col = col;
							(game[r + 1][c]).no += extra;						
						}
						if(r == R-dec && c == C-dec){						
							(game[r - 1][c]).col = col;
							(game[r - 1][c]).no += extra;
							(game[r][c - 1]).col = col;
							(game[r][c - 1]).no += extra;						
						}
                			        game[i][j].no %=2;
						if(game[i][j].no == 0)
							game[i][j].col = ' ';
                       			 }
				}

				else if( (i == 1 && (j != 1 || j != C-dec) ) || (i == R-dec && (j != 1 || j != C-dec) ) || (j == 1 &&(i != 1 || i != R-dec) ) || (j == C-dec &&(i != 1 || i != R-dec))){
					if(game[i][j].no > 2){
						flag = 0;
             				        extra = game[i][j].no / 3;
		        	                /*game[i - 1][j].no += extra;
						game[i - 1][j].col  = col;
                			        game[i + 1][j].no += extra;
						game[i + 1][j].col = col;
                			        game[i][j - 1].no += extra;
						game[i][j - 1].col = col;
                			        game[i][j + 1].no += extra;
						game[i][j + 1].col = col;*/
						if(i == 1 && (j != 1 || j != C-dec) ){
							game[i][j - 1].no += extra;
							game[i][j - 1].col  = col;
                			        	game[i + 1][j].no += extra;
							game[i + 1][j].col = col;
                			        	 game[i][j + 1].no += extra;
							game[i][j + 1].col = col;
						
						}
						if(i == R-dec && (j != 1 || j != C-dec) ){
							game[i - 1][j].no += extra;
							game[i - 1][j].col  = col;
                			        	game[i][j + 1].no += extra;
							game[i][j + 1].col = col;
                			        	 game[i][j - 1].no += extra;
							game[i][j - 1].col = col;
						
						}
						if(j == 1 &&(i != 1 || i != R-dec)){
							  game[i + 1][j].no += extra;
							game[i + 1][j].col = col;
                			        	game[i - 1][j].no += extra;
							game[i - 1][j].col = col;
                			        	game[i][j + 1].no += extra;
							game[i][j + 1].col = col;
						
						}
						if(j == C-dec &&(i != 1 || i != R-dec)){
							  game[i - 1][j].no += extra;
							game[i - 1][j].col = col;
                			        	game[i][j - 1].no += extra;
							game[i][j - 1].col = col;
                			        	game[i + 1][j ].no += extra;
							game[i + 1][j].col = col;
						
						}
                			        
                			        game[i][j].no %=3;
						if(game[i][j].no == 0)
							game[i][j].col = ' ';
					}
				}
				/*Logic For General Boxes...*/
                       		else if(i != 1 && i != R-dec && j != 1 && j != C-dec){
					if(game[i][j].no > 3){
	                        		flag = 0;
	             			        extra = game[i][j].no / 4;
			                        game[i - 1][j].no += extra;
						game[i - 1][j].col  = col;
	                		        game[i + 1][j].no += extra;
						game[i + 1][j].col = col;
	                		        game[i][j - 1].no += extra;
						game[i][j - 1].col = col;
	                		        game[i][j + 1].no += extra;
						game[i][j + 1].col = col;
                		        	game[i][j].no %=4;
						if(game[i][j].no == 0)
							game[i][j].col = ' ';
                       			 }
				}
                   	}
		}
       }
          while(safe > 9999){
		if(col == 'B'){
			print(game, 'B');
			printf("Player2 Has Won!!!\n");
			menu(game, ' ');
		}
		if(col == 'R'){
			print(game, 'R');
			printf("Player1 Has Won!!!\n");
			menu(game, ' ');
		}
	}
}
int put(sq game[R][C], char col, int r, int c){
	int dec = R - game[1][1].dec;
	if((col == 'R' || col == 'B') && (r <= R-dec && c <= C-dec && r > 0 && c > 0)){
		int m, n, chk = 0;
		m = r; n = c;
		if(col == 'B'){
			if ( !(isR(game, m, n))){
				(game[r][c]).col = col;
	    			(game[r][c]).no++;
				char chang = 'B';
				split4(game, chang);
				return 1;
			}
			else {
				printf(ANSI_COLOR_BLUE "already other color exist plz choose different position" ANSI_COLOR_RESET "\n");
				return 0;
			}
		}
		if(col == 'R'){
			if ( !(isB(game, m, n))){
				(game[r][c]).col = col;
	    			(game[r][c]).no++;
				char changR = 'R';
				split4(game, changR);
				return 1;
			}
			else {
				printf(ANSI_COLOR_RED "already other color exist plz choose different position" ANSI_COLOR_RESET "\n");
				return 0;
			}
		}
	}
	int pi;
	if( r == -1 || c == -1){
		 menu(game, col);
			
	}	
	else {
		printf(ANSI_COLOR_Y "Out Of Range!!! Please Enter Position Between 1 to %d only!" ANSI_COLOR_RESET "\n", R-dec );
		return 0;
	}
}
#include "protocols.h"
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
void save(sq game[R][C], char col){
	int dec = R - game[1][1].dec;
	int fd, r, c;
	sq tmp;
	char name[32];
	printf("turn is %c\n", col);
	printf("Dimension is %d.\n", R-dec);
	printf("Give The Name To the Savegame:\n");
	scanf("%s", name);
	fd = open(name, O_WRONLY| O_CREAT, S_IRUSR | S_IWUSR);
	write(fd, &col, 1);
	write(fd, &dec, 4);
	for(r = 1; r <= R-dec; r++)
		for(c = 1; c <= C-dec; c++){
			tmp = game[r][c];
			write(fd, &tmp, sizeof(sq));
				
		}
	close(fd);
	exit(1);
}
int continu(sq game[R][C], char col){
	int dec = R - game[1][1].dec;
	if(col == 'R'){
		int i, j, r, c, flag = 0;
		char ch1, ch2;
		print(game, 'R');
		printf("Player1 is Red Team:\n");
		//first chance to make player1 move(Red)...
			ch1 = 'R';
			printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
			printf(ANSI_COLOR_RED "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_RED "coloumn : " ANSI_COLOR_RESET "\n");
			scanf("%d", &c);
		flag = put(game, ch1, r, c);
		while(!flag){
			printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
			printf(ANSI_COLOR_RED "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_RED "coloumn :  " ANSI_COLOR_RESET"\n");
			scanf("%d", &c);
			flag = put(game, ch1, r, c);
		}
		print(game, 'B' );
		// Second chance to make player2 move(Blue)...
			printf(ANSI_COLOR_BLUE "Player2 is Blue Team:\n");
			ch2 = 'B';
			printf(ANSI_COLOR_BLUE "color : %clue\n", ch2);
			//scanf("%c", &ch2);				
			printf(ANSI_COLOR_BLUE "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_BLUE "coloumn : " ANSI_COLOR_RESET "\n");
			scanf("%d", &c);
		flag  = put(game, ch2, r, c);
		while(!flag){
			printf(ANSI_COLOR_BLUE "Color : %clue\n", ch2);
			printf(ANSI_COLOR_BLUE "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_BLUE "coloumn :" ANSI_COLOR_RESET "\n");
			scanf("%d", &c);
			flag = put(game, ch2, r, c);
		}
		print(game, 'R' );
		return 0;
	}
	if(col == 'B'){
		int i, j, r, c, flag = 0;
		char ch1, ch2;
		print(game, 'B' );
		// Second chance to make player2 move(Blue)...
			printf(ANSI_COLOR_BLUE "Player2 is Blue Team:\n");
			ch2 = 'B';
			printf(ANSI_COLOR_BLUE "color : %clue\n", ch2);
			//scanf("%c", &ch2);				
			printf(ANSI_COLOR_BLUE "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_BLUE "coloumn : " ANSI_COLOR_RESET "\n");
			scanf("%d", &c);
		flag  = put(game, ch2, r, c);
		while(!flag){
			printf(ANSI_COLOR_BLUE "Color : %clue\n", ch2);
			printf(ANSI_COLOR_BLUE "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_BLUE "coloumn :" ANSI_COLOR_RESET "\n");
			scanf("%d", &c);
			flag = put(game, ch2, r, c);
		}

		print(game, 'R');
		printf("Player1 is Red Team:\n");
		//first chance to make player1 move(Red)...
			ch1 = 'R';
			printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
			printf(ANSI_COLOR_RED "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_RED "coloumn : " ANSI_COLOR_RESET "\n");
			scanf("%d", &c);
		flag = put(game, ch1, r, c);
		while(!flag){
			printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
			printf(ANSI_COLOR_RED "row :  ");
			scanf("%d", &r);
			printf(ANSI_COLOR_RED "coloumn :  " ANSI_COLOR_RESET"\n");
			scanf("%d", &c);
			flag = put(game, ch1, r, c);
		}
		print(game, 'B' );
				return 1;
	}	
}
int load(sq game[R][C]){
	int fd, r, c;
	int dec ;
	char name[32], color;
	sq tmp;
	printf("Give The Name of the Savegame:\n");
	scanf("%s", name);
	fd = open(name, O_RDONLY| O_CREAT, S_IRUSR | S_IWUSR);
	read(fd, &color, 1);
	read(fd, &dec, 4);
	game[1][1].dec = dec;
	for(r = 1; r <= R-dec; r++)
		for(c = 1; c <= C-dec; c++){
			read(fd, &tmp, sizeof(sq));
			game[r][c] = tmp;
				
		}
	/*if(color == 'R')
		print(game, 'B');
	else    print(game, 'R');*/
	//continu(game, color);
	printf("Dimension OF loaded game is %d...\n",r-dec);
	close(fd);
	printf("Game is loaded successfully !\n");
	return continu(game, color);
}

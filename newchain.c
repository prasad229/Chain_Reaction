#include<stdio.h>
#include "protocols.h"
#include <stdlib.h>
//Function Definitions...
int isR(sq game[R][C], int r, int c){
	int isR = 0;
	if(game[r][c].col == 'R')
		return 1;
	else return 0;
}
int isB(sq game[R][C], int r, int c){
	int isB = 0;
	if(game[r][c].col == 'B')
		return 1;
	else return 0;
}

int existR(sq game[R][C]){
	int exR = 0, i, j;
	int dec = R - game[1][1].dec;
	for (i = 1; i <= R - dec; i++){
        	for(j = 1; j <= C - dec; j++){
        	    if((game[i][j]).col == 'R')
			return 1;
        	}
    	}
	return 0;
}
int existB(sq game[R][C]){
	int exC = 0, i, j;
	int dec = R - game[1][1].dec;
	for (i = 0; i <= R - dec; i++){
		for(j = 0; j <= C - dec; j++){
			 if((game[i][j]).col == 'B')
				return 1;
		}
	}
	return 0;
}
void print(sq game[R][C], char col){
   	int i, j, m;
	int dec = R - game[1][1].dec;
	if (col == 'B'){
		printf("   ");
		printf(ANSI_COLOR_BLUE "   | ");
		for (i = 1; i <= R-dec ; i++){
			if(i == R-dec)
				printf(ANSI_COLOR_BLUE" %d  |\n", i);
			else if(i > 9) 
				printf(ANSI_COLOR_BLUE" %d | ", i);
			else 	printf(ANSI_COLOR_BLUE" %d  | ", i);
		}
		printf("   ");
		for(i = -3 ; i < ((C-dec)*6) + 1 ; i++){
			if(i%6 == 0)
				printf(ANSI_COLOR_BLUE"|");
			else printf(ANSI_COLOR_BLUE"-");
		}
		printf("\n");
    		for (i = 1; i <= R-dec; i++){
			if(i > 9)
				printf(ANSI_COLOR_BLUE"   %d | ", i);	
			else printf(ANSI_COLOR_BLUE"   %d  | ", i);
        		for(j = 1; j <= C-dec; j++){
				if(game[i][j].col == 'B'){
					printf(ANSI_COLOR_BLUE "%c" , (game[i][j]).col );
        		    		printf(ANSI_COLOR_BLUE "%d", (game[i][j]).no );
					printf(ANSI_COLOR_BLUE "  | ");
	
				}
				if(isR(game, i, j)){
					printf(ANSI_COLOR_RED "%c" , (game[i][j]).col );
        		    		printf(ANSI_COLOR_RED "%d" , (game[i][j]).no );
					printf(ANSI_COLOR_BLUE "  | ");
	
				}
				
				else if (!isR(game, i, j) && !isB(game, i, j)) {
					printf( ANSI_COLOR_RESET "%c", (game[i][j]).col);
        		    		printf(ANSI_COLOR_RESET "%d", (game[i][j]).no);
					printf(ANSI_COLOR_BLUE"  | ");
				}
				
        		}
        		printf("\n");
			printf("   ");
			for(m = -3 ; m < ((C-dec)*6) + 1 ; m++){
				if( m == ((C-dec)*6))
					printf(ANSI_COLOR_BLUE "|\n");
				else if(m % 6 == 0)
					printf(ANSI_COLOR_BLUE "|");
				else printf(ANSI_COLOR_BLUE "-");
			}
    		}
	}
	if (col == 'R'){
		printf("   ");
		printf(ANSI_COLOR_RED "   | ");
		for (i = 1; i <= R-dec; i++){
			if(i == R-dec)
				printf(ANSI_COLOR_RED " %d  |\n", i);
			else if (i > 9)
				printf(ANSI_COLOR_RED " %d | ", i);
			else	printf(ANSI_COLOR_RED " %d  | ", i);
		}
		printf("   ");
		for(i = -3 ; i < ((C-dec)*6) + 1 ; i++){
			if(i%6 == 0)
				printf(ANSI_COLOR_RED "|");
			else printf(ANSI_COLOR_RED "-");
		}
		printf("\n");
		for (i = 1; i <= R-dec; i++){
			if(i > 9)
				printf(ANSI_COLOR_RED "   %d | ", i);
    		
			else printf(ANSI_COLOR_RED "   %d  | ", i);
        		for(j = 1; j <= C-dec; j++){
				if(game[i][j].col == 'B'){
					printf(ANSI_COLOR_BLUE "%c" , (game[i][j]).col );
        		    		printf(ANSI_COLOR_BLUE "%d", (game[i][j]).no );
					printf(ANSI_COLOR_RED  "  | ");
	
				}
				if(isR(game, i, j)){
					printf(ANSI_COLOR_RED "%c" , (game[i][j]).col );
        		    		printf(ANSI_COLOR_RED "%d" , (game[i][j]).no );
					printf(ANSI_COLOR_RED  "  | ");
	
				}
				
				else if (!isR(game, i, j) && !isB(game, i, j)) {
					printf( ANSI_COLOR_RESET "%c", (game[i][j]).col);
        		    		printf(ANSI_COLOR_RESET "%d", (game[i][j]).no);
					printf(ANSI_COLOR_RED"  | ");
				}
				
        		}
        		printf("\n");
			printf("   ");
			for(m = -3 ; m < ((C-dec)*6) + 1 ; m++){
				if( m == ((C-dec)*6))
					printf(ANSI_COLOR_RED "|\n");
				else if(m % 6 == 0)
					printf(ANSI_COLOR_RED "|");
				else printf(ANSI_COLOR_RED "-");
			}
    		}
	}	
}
int menu(sq game[R][C], char col){
	int choice;
	printf(ANSI_COLOR_Y "Select Appropriate Option:\n" ANSI_COLOR_RESET);
	printf(ANSI_COLOR_G "0. Play With Computer:\n" ANSI_COLOR_RESET);
	printf(ANSI_COLOR_G "1. Resume :\n" ANSI_COLOR_RESET);
	printf(ANSI_COLOR_G "2. New Game :\n" ANSI_COLOR_RESET);
	printf(ANSI_COLOR_G "3. Save Game :\n" ANSI_COLOR_RESET);
	printf(ANSI_COLOR_G "4. Load game :\n" ANSI_COLOR_RESET);
	printf(ANSI_COLOR_G "5. Exit Game:\n" ANSI_COLOR_RESET);
	
	scanf("%d", &choice);
	switch(choice){
		case 0:
			return 0;
			break;
		case 1: 
			/*if(col == 'R')
				print(game, 'R');
			else if(col == 'B')*/
				//print(game, col);
			return 1;
			break;
		case 2: 
			start(game);
			return 2;
			break;
		case 3:
			save(game, col);
			break;
			return 3;
		case 4:
			//load(game);
			return 4;
			break;
		case 5: 
			Exit(game, col);
			break;
			return 5;

		
	}
}
void start(sq game[R][C]){
      	int i, j, r, c, flag = 0;
	char ch1, ch2;
	int dec, dimflag = 1 ;
	printf(ANSI_COLOR_G "Enter the dimensions of the board:\n" ANSI_COLOR_RESET);
	while(dimflag){
	//scanf("%d", &game[1][1].dec);
		scanf("%d", &game[1][1].dec);
		dimflag = 0;
		if(game[1][1].dec < 2 || game[1][1].dec > 15){
			printf(ANSI_COLOR_Y "Dimensions should be between 2 to 15 for better gaming experience.\n Enter Again!\n" ANSI_COLOR_RESET);
			dimflag = 1;
		}
	}
	dec = R - game[1][1].dec;
    	for (i = 1; i <= R - dec; i++){
        	for(j = 1; j <= C - dec; j++){
        	    	(game[i][j].no) = 0;
			(game[i][j].col) = ' ';
       		 }
    	}
	printf(ANSI_COLOR_RED "Enter Player1's Name :-\n" ANSI_COLOR_RESET);
	scanf("%s", play1);
	printf(ANSI_COLOR_BLUE "Enter Player2's Name :-\n" ANSI_COLOR_RESET);
	scanf("%s", play2);	
	print(game, 'R');
	printf("%s is Red Team:\n", play1);
	//first chance to make player1 move(Red)...
		ch1 = 'R';
		printf("Press -1 to go to the Menu:\n");
		printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
		printf(ANSI_COLOR_RED "row :  ");
		scanf("%d", &r);
		printf(ANSI_COLOR_RED "coloumn : " ANSI_COLOR_RESET "\n");
		scanf("%d", &c);
	flag = put(game, ch1, r, c);
	while(!flag){
		printf("Press -1 to go to the Menu:\n");
		printf(ANSI_COLOR_RED "Color : %ced\n", ch1);
		printf(ANSI_COLOR_RED "row :  ");
		scanf("%d", &r);
		printf(ANSI_COLOR_RED "coloumn :  " ANSI_COLOR_RESET"\n");
		scanf("%d", &c);
		flag = put(game, ch1, r, c);
	}
	print(game, 'B' );
	// Second chance to make player2 move(Blue)...
		printf(ANSI_COLOR_BLUE "%s is Blue Team:\n", play2);
		ch2 = 'B';
		printf("Press -1 to go to the Menu:\n");
		printf(ANSI_COLOR_BLUE "color : %clue\n", ch2);
		//scanf("%c", &ch2);				
		printf(ANSI_COLOR_BLUE "row :  ");
		scanf("%d", &r);
		printf(ANSI_COLOR_BLUE "coloumn : " ANSI_COLOR_RESET "\n");
		scanf("%d", &c);
	flag  = put(game, ch2, r, c);
	while(!flag){
		printf("Press -1 to go to the Menu:\n");
		printf(ANSI_COLOR_BLUE "Color : %clue\n", ch2);
		printf(ANSI_COLOR_BLUE "row :  ");
		scanf("%d", &r);
		printf(ANSI_COLOR_BLUE "coloumn :" ANSI_COLOR_RESET "\n");
		scanf("%d", &c);
		flag = put(game, ch2, r, c);
	}
	print(game, 'R' );

}
void Exit(sq game[R][C],char col){
	char ans, sav;
	printf(ANSI_COLOR_Y "Do you want to save the game before quiting?(Y/N)?\n" ANSI_COLOR_RESET);
	 printf(ANSI_COLOR_G "\tYES(Y/y)\t" ANSI_COLOR_RED "No(N/n)\n" ANSI_COLOR_RESET);
		scanf("%c", &sav);
		scanf("%c", &sav);
	if(sav == 'y' || sav == 'Y')
		save(game, col);
	else{
		loop:
		printf(ANSI_COLOR_Y "Do you really want to quit the game (Y/N)?\n" ANSI_COLOR_RESET);
		 printf(ANSI_COLOR_G "\tYES(Y/y)\t" ANSI_COLOR_RED "No(N/n)\n" ANSI_COLOR_RESET);
		scanf("%c", &ans);
		scanf("%c", &ans);
		if(ans == 'Y' || ans == 'y')	
			exit(1);
		else if(ans == 'N' || ans == 'n')
			menu(game, ' ');
		else {

			printf(ANSI_COLOR_Y "Please Enter Y or N\n" ANSI_COLOR_RESET);
			goto loop;
		}
	}
}

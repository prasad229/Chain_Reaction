					        *****Readme for Chain-Reaction*****
---------------------------------------------------------------------------------------------------------------------------------------------

	This game is exclusively made for UBUNTU platform. This is the only requirement for this simple game.

 #RULES :
 
	1. This game can be played for m x n board but most commonly used size is 9 x 9. Thus this game is available in  n x n board; 
	   you can select any board upto dimension of 15; But I recommend the you to select the dimensions between 6 to 9.

	2. This game is made for two modes:
		a) Two Player Mode         b) Single Player Mode (Play With Computer)

	3. The Cell in which you want to put your orb , you are needed to put its co-ordinates. Your colour is given by character and 
	   number of orbs are mentioned in the cell. 

	4. All cells are initially empty. The Red and Blue player takes turn to place "Orbs" of their corresponding colors. The Red player
	   can place only in an empty cell or in the cell which already contains one or more Red orbs. When two or more orbs are placed in 
	   the same cell, they are stacked up.

	5. For each cell in the board we define crirical mass. The critical mass is eequal to the number of orthogonally adjacent cells.
	   that would be 4 for usual cells, 3 for cells in the edge and 2 for cells in the corners.

	6. When the cell is loaded with number of orbs equal to its critical mass, the stack immidiately explodes. As a result, all the 
	   orthogonally adjacent are added with one orb and colour also changes to same as of exploding cell. The explosion may result into 
	   overloading of an adjacent cell and the chain reaction of explosion continues till all the cells become stable.

	7. The Winner is the one who eliminates every other player's Orbs.

 #Options:
	
	1. Game can be started by compiling and linking all the .c and .h files and executing by using command './a.out'.

	2. You can save the game at any point of time and saving game needs special savefile name of your choice which you are needed to
	   re-enter when you want to load that game again.

 	3. You can go to menu by entering '-1'.	


				                *******Enjoy The Game & Thank You!*******